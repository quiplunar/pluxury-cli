#!/usr/bin/env node

const commander = require('commander')

const fs = require('fs')
const AdmZip = require('adm-zip')
const request = require('request')
const mkdirp = require('mkdirp')

const file_url = 'https://gitlab.com/quiplunar/pluxury/-/archive/master/pluxury-master.zip'

commander.version('1.0.0').description('rain - сборщик на webpack')

function templateTs(name) {
  return `export function ${camelize(name)} () {\n}\n`
}

function templateScss(name) {
  return `.${name} {\n}\n`
}

function templateHbs(name) {
  return `<div class="${name}"></div>\n`
}

function addImportTs(name, alias) {
  let array = fs.readFileSync('src/static/scripts/components.js').toString().split('\n').filter(item => item !== '')

  for (let i = array.length - 1; i >= 0; i--) {
    if (String(array[i]).match('}')) {
      array.splice((i),0,`  ${camelize(name)}()`)
      break
    }
  }

  for (let i = 0; i < array.length; i++) {
    if ( String(array[i]).match('export function') ) {
      array.splice((i),0,`import { ${camelize(name)} } from '@${alias}/${name}/${name}'\n`)
      break
    }
  }

  array.splice(array.length, 0, '')

  fs.writeFileSync('src/static/scripts/components.js', array.join('\n'))
}

function addImportScss(name, path) {
  fs.appendFileSync(`src/static/styles/${path}.scss`, `@import '${path}/${name}/${name}';\n`)
}

function camelize(str) {
  return str
    .split('-')
    .map(
      (word, index) => index === 0 ? word : word[0].toUpperCase() + word.slice(1)
    )
    .join('')
}

commander
  .command('init')
  .description('Создание чистого проекта')
  .alias('i')
  .action(() => {
    request.get({url: file_url, encoding: null}, (err, res, body) => {
      const zip = new AdmZip(body)
      const zipEntries = zip.getEntries()

      for (let i = 0; i < zipEntries.length; i++) {
        zipEntries[i].entryName = zipEntries[i].entryName.replace('pluxury-master', '/')
      }

      zip.extractAllTo('', true)
    })
  })

commander
  .command('component <name>')
  .description('Создание компонента')
  .alias('c')
  .action((name) => {
    mkdirp.sync(`src/components/${name}`)

    fs.writeFileSync(`src/components/${name}/${name}.scss`, templateScss(name))
    fs.writeFileSync(`src/components/${name}/${name}.js`, templateTs(name))
    fs.writeFileSync(`src/components/${name}/${name}.hbs`, templateHbs(name))

    addImportTs(name, 'components')
    addImportScss(name, 'components')
  })

commander
  .command('page <name>')
  .description('Создание страницы')
  .alias('p')
  .action((name) => {
    mkdirp.sync(`src/pages/${name}`)

    fs.writeFileSync(`src/pages/${name}/${name}.scss`, templateScss(name))
    fs.writeFileSync(`src/pages/${name}/${name}.js`, templateTs(name))
    fs.writeFileSync(`src/pages/${name}/${name}.hbs`, templateHbs(name))

    addImportTs(name, 'pages')
    addImportScss(name, 'pages')
  })

commander.parse(process.argv)
